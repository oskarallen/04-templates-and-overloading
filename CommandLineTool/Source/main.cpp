//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>

int max (int a, int b)
{
    if (a < b)
    {
        return b;
    }
    else
    {
        return b;
    }
}
double max (double a, double b)
{
    if (a < b)
    {
        return b;
    }
    else
    {
        return b;
    }
}
    
int main()
{
    int iMaxInt = max(2, 8);
    std::cout << "Max Integer = " << iMaxInt;
    
    double dMaxDouble = max(3.45634, 6.23532);
    std:: cout << "\nMax Double = " << dMaxDouble;
    
    return 0;
}
